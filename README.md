# ** # NET.Core 2.0 / Angular5 / Angular CLI / Webpack3 / Covalent UI / Angular Material2 / Boostrap 4 / ng-bootstrap / Typescript 2 / SASS ** #

1. NET.Core SDK
2. VS2017
3. NodeJS >= 6.9 and NPM >= 4


### Using
1. dotnet restore
2. rimraf node_modules
3. npm install
4. npm run dev:watch
5. npm run publish

### Commands
```
"dev:watch": "set ASPNETCORE_ENVIRONMENT=Development && dotnet watch run",
"prod:watch": "set ASPNETCORE_ENVIRONMENT=Production && dotnet watch run",
"clean": "rimraf bin wwwroot/dist"
```

Browse using http://localhost:5000

### Screenshots
![Start](/images/2018-04-24_20_34_51_start.png)
![Enter number](/images/2018-04-24_20_35_42_number.png)
![Summary part 1](/images/2018-04-24_20_36_34_summary.png)
![Summary part 2](/images/2018-04-24_20_36_34_summary2.png)

### Components

* https://material.angular.io/components
* https://teradata.github.io/covalent/
* https://material.angular.io/components/
* https://materialdesignicons.com/
* https://www.materialui.co/colors/
