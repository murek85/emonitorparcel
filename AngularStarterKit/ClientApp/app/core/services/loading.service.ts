import { Injectable, EventEmitter } from '@angular/core';

import { TdLoadingService } from '@covalent/core';

@Injectable()
export class LoadingService {
	loading: EventEmitter<boolean> = new EventEmitter<boolean>();

	constructor(private loadingService: TdLoadingService) {

	}

	public start() {
		this.loadingService.register();
		this.loading.emit(true);
	}

	public stop() {
		this.loadingService.resolve();
		this.loading.emit(false);
	}
}
