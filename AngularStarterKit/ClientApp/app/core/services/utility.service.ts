import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class UtilityService {
    router: Router;

    constructor(router: Router) {
        this.router = router;
    }

    navigate(path: string) {
        this.router.navigate([path]);
    }

    navigateToSignIn() {
        this.navigate('/login');
    }
}
