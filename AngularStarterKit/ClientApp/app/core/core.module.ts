﻿import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { LoadingService } from './services/loading.service';
import { UtilityService } from './services/utility.service';

@NgModule({
    imports: [
        CommonModule
    ]
})
export class CoreModule {
    // forRoot allows to override providers
    // https://angular.io/docs/ts/latest/guide/ngmodule.html#!#core-for-root
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                LoadingService,
                UtilityService
            ]
        };
    }
}
