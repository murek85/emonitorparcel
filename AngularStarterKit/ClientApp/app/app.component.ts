﻿import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { TdMediaService, TdRotateAnimation } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '@app/router.animations';

@Component({
  selector: 'appc-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
  animations: [routerTransition, TdRotateAnimation()]
})
export class AppComponent implements AfterViewInit {

	constructor(public media: TdMediaService,
		public _translateService: TranslateService,
		private _changeDetectorRef: ChangeDetectorRef) {}

	public ngAfterViewInit(): void {
		// broadcast to all listener observables when loading the page
		this.media.broadcast();
		this._changeDetectorRef.detectChanges();
	}

	public getState(outlet: any) {
		return outlet.activatedRouteData.state;
	}
}
