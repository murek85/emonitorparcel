﻿import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
    {
        path: '', redirectTo: 'home', pathMatch: 'full'
    },
    // Lazy async modules
    {
        path: 'login', loadChildren: './components/login/login.module#LoginModule'
    },
    {
        path: 'home', loadChildren: './components/home/home.module#HomeModule'
    }
];

export const routing = RouterModule.forRoot(routes, {
  preloadingStrategy: PreloadAllModules,
  // enableTracing: true
});
