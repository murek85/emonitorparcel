﻿import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';

import { TranslateModule } from '@ngx-translate/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxMGaugeModule } from 'ngx-mgauge';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { AvatarModule } from 'ng2-avatar';
import { VirtualScrollModule } from 'angular2-virtual-scroll';
import { NgxMaskModule } from 'ngx-mask';
import { ArchwizardModule } from 'angular-archwizard';

// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Covalent
import {
    CovalentCommonModule,
    CovalentMenuModule,
    CovalentLayoutModule,
    CovalentNotificationsModule,
    CovalentMediaModule,
    CovalentStepsModule,
    CovalentChipsModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentJsonFormatterModule,
    CovalentSearchModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentExpansionPanelModule,
    CovalentMessageModule
} from '@covalent/core';
// (optional) Additional Covalent Modules imports
import { CovalentHttpModule } from '@covalent/http';
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';
import { CovalentDynamicFormsModule } from '@covalent/dynamic-forms';

// Angular Material
import {
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    // MatNativeDateModule,
    // NativeDateAdapter,
    // DateAdapter,
    // MAT_DATE_FORMATS,
    // MAT_NATIVE_DATE_FORMATS
} from '@angular/material';

// Components
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { SubheaderComponent } from './layout/subheader/subheader.component';

const ANGULAR_MODULES: any[] = [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    TranslateModule
];

const MATERIAL_MODULES: any[] = [
    MatAutocompleteModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
];

const COVALENT_MODULES: any[] = [
    CovalentCommonModule,
    CovalentMenuModule,
    CovalentLayoutModule,
    CovalentNotificationsModule,
    CovalentLayoutModule,
    CovalentMediaModule,
    CovalentStepsModule,
    CovalentChipsModule,
    CovalentPagingModule,
    CovalentDataTableModule,
    CovalentExpansionPanelModule,
    CovalentMessageModule,
    CovalentHttpModule,
    CovalentHighlightModule,
    CovalentMarkdownModule,
    CovalentDynamicFormsModule,
    CovalentSearchModule,
    CovalentDialogsModule,
    CovalentLoadingModule,
    CovalentJsonFormatterModule
];

const BOOTSTRAP_MODULES: any[] = [
    NgbModule
];

const CUSTOM_MODULES: any[] = [
    NgxChartsModule,
    NgxMGaugeModule,
    AngularOpenlayersModule,
    AvatarModule,
	VirtualScrollModule,
	ArchwizardModule
];

const COMPONENTS: any[] = [
    HeaderComponent,
    FooterComponent,
    SubheaderComponent
];

const DIALOGS: any[] = [];

const PIPES: any[] = [];

const DIRECTIVES: any[] = [];

@NgModule({
    imports: [
        ANGULAR_MODULES,
        MATERIAL_MODULES,
        COVALENT_MODULES,
        BOOTSTRAP_MODULES,
        CUSTOM_MODULES,

        NgxMaskModule.forRoot(),
    ],
    declarations: [
        COMPONENTS,
        DIALOGS,
        PIPES,
        DIRECTIVES
    ],
    exports: [
        ANGULAR_MODULES,
        MATERIAL_MODULES,
        COVALENT_MODULES,
        BOOTSTRAP_MODULES,
        CUSTOM_MODULES,
        COMPONENTS,
        DIALOGS,
        PIPES,
        DIRECTIVES,

        NgxMaskModule
    ],
    entryComponents: [
        DIALOGS
    ],
    providers: [
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        // { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
        // { provide: DateAdapter, useClass: NativeDateAdapter },
        // { provide: MAT_DATE_FORMATS, useValue: MAT_NATIVE_DATE_FORMATS },
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
        ngModule: SharedModule,
        providers: []
    };
  }
}
