export interface LanguageModel {
    id?: number;
    symbol: string;
    name: string;
}
