﻿import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

import { HomeComponent } from './home.component';
import { routing } from './home.routes';

@NgModule({
    imports: [
        routing,
        SharedModule
    ],
    declarations: [HomeComponent]
})
export class HomeModule { }
