﻿import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TdLoadingService } from '@covalent/core';

@Component({
    selector: 'appc-home-component',
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
	carriers: any = [
		{ name: 'DHL', value: 'dhl', icon: 'dist/assets/dhl.png', active: false },
		{ name: 'DPD', value: 'dpd', icon: 'dist/assets/dpd.png', active: false },
		{ name: 'FedEx', value: 'fedex', icon: 'dist/assets/fedex.png', active: false },
		{ name: 'GLS', value: 'gls', icon: 'dist/assets/gls.png', active: false },
		{ name: 'Inpost', value: 'inpost', icon: 'dist/assets/inpost.png', active: true },
		{ name: 'K-EX', value: 'kex', icon: 'dist/assets/kex.png', active: false },
		{ name: 'Paczkomaty Inpost', value: 'paczkomaty', icon: 'dist/assets/paczkomaty.png', active: true },
		{ name: 'Poczta Polska', value: 'pocztapolska', icon: 'dist/assets/pocztapolska.png', active: true },
		{ name: 'UPS', value: 'ups', icon: 'dist/assets/ups.png', active: false },
    ];

    //route: any = [
    //    { datetime: '14-04-2018, 11:23', status: 'Dostarczona', description: 'Podróż przesyłki od Nadawcy do Odbiorcy zakończyła się, ale nie musi to oznaczać końca naszej znajomości:) Jeśli lubisz InPost, odwiedź nasz fanpage na Facebooku. Dziękujemy!' },
    //    { datetime: '13-04-2018, 23:45', status: 'Umieszczona w paczkomacie (odbiorczym)', description: 'I gotowe! Paczka poczeka na odbiór 48 godzin od momentu umieszczenia w Paczkomacie. Odbiorca otrzymuje SMS-a i e-maila z kodem odbioru. Jeśli paczka nie zostanie odebrana w tym czasie, trafi do najbliższego oddziału InPost, o czym poinformujemy Odbiorcę w osobnym SMS-ie i e-mailu.' },
    //    { datetime: '13-04-2018, 21:16', status: 'Przekazano do doręczenia', description: 'Przesyłka trafi do odbiorcy najpóźniej w najbliższym dniu roboczym. Doręczyciel InPost rozwozi przesyłki nawet do późnych godzin wieczornych, dlatego warto mieć włączony telefon.' },
    //    { datetime: '13-04-2018, 03:53', status: 'Przyjęta w oddziale Inpost', description: 'Przesyłka trafiła do oddziału InPost, skąd wkrótce wyruszy w dalszą drogę.' },
    //    { datetime: '13-04-2018, 00:07', status: 'W trasie', description: 'Przesyłka jest transportowana między oddziałami InPost.' },
    //    { datetime: '12-04-2018, 26:06', status: 'W trasie', description: 'Przesyłka jest transportowana między oddziałami InPost.' },
    //    { datetime: '12-04-2018, 18:23', status: 'W trasie', description: 'Przesyłka jest transportowana między oddziałami InPost.' },
    //    { datetime: '12-04-2018, 17:00', status: 'Przyjęta w oddziale Inpost', description: 'Przesyłka trafiła do oddziału InPost, skąd wkrótce wyruszy w dalszą drogę.' },
    //    { datetime: '12-04-2018, 14:25', status: 'Odebrana od klienta', description: 'Kurier odebrał paczkę od Nadawcy i przekazuje ją do oddziału InPost.' },
    //    { datetime: '12-04-2018, 10:29', status: 'Przygotowana przez nadawcę', description: 'Nadawca poinformował nas, że przygotował przesyłkę do nadania. Podróż przesyłki jeszcze się nie rozpoczęła.' }
    //];

	carrier: any;
	number: string;
	routes: any[];
	route: any;
	statuses: any[];
	address: any;
	error: any;

	constructor(private httpClient: HttpClient,
		private loadingService: TdLoadingService) { }

	ngOnInit() {

		this.number = null;
		this.routes = [];
		this.route = {
			status: {}
		};
		this.statuses = [];
		this.address = {};
		this.error = {};
	}

	checkCarrier(carrier: any) {
		console.log(carrier);
	}

	checkShipmentNumber(number: string) {
		console.log(number);

		switch (this.carrier.value) {
			case 'inpost':
			case 'paczkomaty': {
				this.initInpost();
				this.prepareInpost(number);
				break;
            }
            case 'pocztapolska': {
                this.preparePocztaPolska(number);
                break;
            }
		}
	}

	initInpost() {
		this.statuses = [];
		this.routes = [];
		this.route = {
			status: {}
		};
		this.address = {};
		this.error = {};

		this.loadingService.register('overlayStarSyntax');
		this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/statuses')
			.toPromise()
			.then((response: any) => {
				console.log(response);

				response.items.forEach((item: any) => {
					this.statuses.push({
						name: item.name,
						title: item.title,
						description: item.description
					});
				});

				this.loadingService.resolve('overlayStarSyntax');
			})
			.catch(err => {
				console.log(err);

				this.loadingService.resolve('overlayStarSyntax');
			});
	}

	prepareInpost(number: string) {
		// 631222102841813014203251 - odebrana
		// 631586108841813034113332
		this.loadingService.register('overlayStarSyntax');
		this.httpClient.get('https://api-shipx-pl.easypack24.net/v1/tracking/' + number)
			.toPromise()
			.then((response: any) => {
				console.log(response);

				response.tracking_details.forEach((item: any) => {
					this.routes.push({
						datetime: item.datetime,
						status: this.statuses.find((status: any) => status.name === item.status)
					});
				});

				this.route = this.routes.find((route: any) => route.status.name === response.status);
				this.address = {
					line1: `${response.custom_attributes.target_machine_detail.address.line1}`,
					line2: `${response.custom_attributes.target_machine_detail.address.line2}`,
					description: response.custom_attributes.target_machine_detail.location_description
				};

				this.loadingService.resolve('overlayStarSyntax');
			})
			.catch(err => {
				console.log(err);

				this.error = {
					name: err.name,
					ok: err.ok,
					status: err.status,
					statusText: err.statusText
				};

				this.loadingService.resolve('overlayStarSyntax');
			});
	}

	initPocztaPolska() {
		this.statuses = [];
		this.routes = [];
		this.route = {
			status: {}
		};
		this.address = {};
		this.error = {};
	}

    preparePocztaPolska(number: string) {
        
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.open('POST', 'https://tt.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl');

        let sr = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sled="http://sledzenie.pocztapolska.pl">
                    <soapenv:Header>
                        <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility1.0.xsd">
                                <wsse:Username>sledzeniepp</wsse:Username>
                                <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">PPSA</wsse:Password>
                            </wsse:UsernameToken>
                        </wsse:Security> 
                    </soapenv:Header>
                    <soapenv:Body>
                        <sled:sprawdzPrzesylke>
                            <sled:numer>` + number + `</sled:numer>
                        </sled:sprawdzPrzesylke>
                    </soapenv:Body>
                </soapenv:Envelope>`;

        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState === 4) {
                if (xmlHttp.status === 200) {
                    let xml = xmlHttp.responseXML;
                    console.log(xml);

                    // TODO: Parser odebranych danych
                }
            }
        };

        xmlHttp.setRequestHeader('Content-Type', 'text/xml');
        xmlHttp.send(sr);
    }

     xml2json(xml: any) {
         try {
            debugger
            let obj: any = {};
            if (xml.children.length > 0) {
                for (let i = 0; i < xml.children.length; i++) {
                    let item = xml.children.item(i);
                    let nodeName = item.nodeName;

                    if (typeof (obj[nodeName]) == "undefined") {
                        obj[nodeName] = this.xml2json(item);
                    } else {
                        if (typeof (obj[nodeName].push) == "undefined") {
                            let old = obj[nodeName];

                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(this.xml2json(item));
                    }
                }
            } else {
                obj = xml.textContent;
            }
            return obj;
        } catch (e) {
            console.log(e.message);
            return null;
        }
    }
}
