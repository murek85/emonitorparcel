﻿using Microsoft.AspNetCore.Identity;

namespace AngularStarterKit.Entities
{
	public class ApplicationUser : IdentityUser<int>
	{
    }
}
