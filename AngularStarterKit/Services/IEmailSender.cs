﻿using System.Threading.Tasks;

namespace AngularStarterKit.Services
{
	public interface IEmailSender
	{
		Task SendEmailAsync(string email, string subject, string message);
	}
}
