using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace AngularStarterKit
{
	public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
					.UseConfiguration(new ConfigurationBuilder()
				  .SetBasePath(Directory.GetCurrentDirectory())
				  .AddJsonFile("hosting.json", optional: true)
				  .Build()
			  )
			  .UseStartup<Startup>()
			  .UseKestrel(a => a.AddServerHeader = false)
			  .Build();
	}
}
