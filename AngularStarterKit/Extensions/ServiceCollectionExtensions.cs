﻿using AspNet.Security.OpenIdConnect.Primitives;
using AngularStarterKit.DbContexts;
using AngularStarterKit.Entities;
using AngularStarterKit.Filters;
using AngularStarterKit.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularStarterKit.Extensions
{
    public static class ServiceCollectionExtensions
    {
		public static IServiceCollection AddSslCertificate(this IServiceCollection services, IHostingEnvironment hostingEnv)
		{
			return services;
		}

		public static IServiceCollection AddCustomizedMvc(this IServiceCollection services)
		{
			services.AddMvc(options =>
			{
				options.Filters.Add(typeof(ModelValidationFilter));
			})
			.AddJsonOptions(options =>
			{
				options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
			});

			return services;
		}

		public static IServiceCollection AddCustomIdentity(this IServiceCollection services)
		{
			// For api unauthorised calls return 401 with no body
			services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
			{
				options.Password.RequiredLength = 4;
				options.Password.RequireNonAlphanumeric = false;
			})
			.AddEntityFrameworkStores<ApplicationDbContext>()
			.AddDefaultTokenProviders();

			return services;
		}

		public static IServiceCollection AddCustomOpenIddict(this IServiceCollection services)
		{
			// Configure Identity to use the same JWT claims as OpenIddict instead
			// of the legacy WS-Federation claims it uses by default (ClaimTypes),
			// which saves you from doing the mapping in your authorization controller.
			services.Configure<IdentityOptions>(options =>
			{
				options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
				options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
				options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
			});

			// Register the OpenIddict services.
			services.AddOpenIddict(options =>
			{
				// Register the Entity Framework stores.
				options.AddEntityFrameworkCoreStores<ApplicationDbContext>();

				// Register the ASP.NET Core MVC binder used by OpenIddict.
				// Note: if you don't call this method, you won't be able to
				// bind OpenIdConnectRequest or OpenIdConnectResponse parameters.
				options.AddMvcBinders();

				// Enable the token endpoint.
				// Form password flow (used in username/password login requests)
				options.EnableTokenEndpoint("/connect/token");

				// Enable the authorization endpoint.
				// Form implicit flow (used in social login redirects)
				options.EnableAuthorizationEndpoint("/connect/authorize");

				// Enable the password and the refresh token flows.
				options.AllowPasswordFlow()
					   .AllowRefreshTokenFlow()
					   .AllowImplicitFlow(); // To enable external logins to authenticate

				options.SetAccessTokenLifetime(TimeSpan.FromMinutes(30));
				options.SetIdentityTokenLifetime(TimeSpan.FromMinutes(30));
				options.SetRefreshTokenLifetime(TimeSpan.FromMinutes(60));
				// During development, you can disable the HTTPS requirement.
				options.DisableHttpsRequirement();

				// Note: to use JWT access tokens instead of the default
				// encrypted format, the following lines are required:
				//
				// options.UseJsonWebTokens();
				options.AddEphemeralSigningKey();
			});

			// If you prefer using JWT, don't forget to disable the automatic
			// JWT -> WS-Federation claims mapping used by the JWT middleware:
			//
			// JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
			// JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap.Clear();
			//
			// services.AddAuthentication()
			//     .AddJwtBearer(options =>
			//     {
			//         options.Authority = "http://localhost:54895/";
			//         options.Audience = "resource_server";
			//         options.RequireHttpsMetadata = false;
			//         options.TokenValidationParameters = new TokenValidationParameters
			//         {
			//             NameClaimType = OpenIdConnectConstants.Claims.Subject,
			//             RoleClaimType = OpenIdConnectConstants.Claims.Role
			//         };
			//     });

			// Alternatively, you can also use the introspection middleware.
			// Using it is recommended if your resource server is in a
			// different application/separated from the authorization server.
			//
			// services.AddAuthentication()
			//     .AddOAuthIntrospection(options =>
			//     {
			//         options.Authority = new Uri("http://localhost:54895/");
			//         options.Audiences.Add("resource_server");
			//         options.ClientId = "resource_server";
			//         options.ClientSecret = "875sqd4s5d748z78z7ds1ff8zz8814ff88ed8ea4z4zzd";
			//         options.RequireHttpsMetadata = false;
			//     });

			services.AddAuthentication(options =>
			{
				// This will override default cookies authentication scheme
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			   .AddOAuthValidation();

			return services;
		}

		public static IServiceCollection AddCustomDbContext(this IServiceCollection services)
		{
			// Add framework services.
			services.AddDbContextPool<ApplicationDbContext>(options =>
			{
				options.UseSqlServer(Startup.Configuration["Data:PostgreSqlConnectionString"]);
				options.UseOpenIddict();
			});
			return services;
		}

		public static IServiceCollection RegisterCustomServices(this IServiceCollection services)
		{
			// New instance every time, only configuration class needs so its ok
			services.AddTransient<IEmailSender, AuthMessageSender>();
			services.AddTransient<ApplicationDbContext>();
			services.AddScoped<UserResolveService>();
			services.AddScoped<ApiExceptionFilter>();
			return services;
		}
	}
}
