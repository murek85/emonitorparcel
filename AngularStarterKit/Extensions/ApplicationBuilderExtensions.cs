﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AngularStarterKit.Extensions
{
	public static class ApplicationBuilderExtensions
    {
		public static IApplicationBuilder UseCustomWebpackDevMiddleware(this IApplicationBuilder app)
		{
			app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
			{
				HotModuleReplacement = true
			});
			return app;
		}

		public static IApplicationBuilder UseCustomSwaggerApi(this IApplicationBuilder app)
		{
			// Enable middleware to serve generated Swagger as a JSON endpoint
			app.UseSwagger();
			// Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
			});

			return app;
		}

		public static IApplicationBuilder AddDevMiddlewares(this IApplicationBuilder app)
		{
			var env = app.ApplicationServices.GetRequiredService<IHostingEnvironment>();
			var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();

			if (env.IsDevelopment())
			{
				loggerFactory.AddConsole(Startup.Configuration.GetSection("Logging"));
				loggerFactory.AddDebug();
				app.UseDeveloperExceptionPage();
				app.UseDatabaseErrorPage();
				app.UseCustomWebpackDevMiddleware();
				// NOTE: For SPA swagger needs adding before MVC
				app.UseCustomSwaggerApi();
			}

			// TODO loggerFactory.AddSerilog();

			return app;
		}
	}
}
