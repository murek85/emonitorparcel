using AngularStarterKit.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace AngularStarterKit
{
	public class Startup
    {
		public static IHostingEnvironment _hostingEnv;

		public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
			Configuration = configuration;
			_hostingEnv = env;

			Helpers.SetupSerilog();
		}

        public static IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddCustomHeaders();

			if (_hostingEnv.IsDevelopment())
			{
				services.AddSslCertificate(_hostingEnv);
			}

			services.AddOptions();
			services.AddResponseCompression(options =>
			{
				options.MimeTypes = Helpers.DefaultMimeTypes;
			});
			services.AddCustomDbContext();
			services.AddCustomIdentity();
			services.AddCustomOpenIddict();
			services.AddMemoryCache();
			services.RegisterCustomServices();
			services.AddSignalR();
			services.AddCustomizedMvc();
			services.AddNodeServices();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "AspNetCoreSpa", Version = "v1" });
			});
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
			app.AddDevMiddlewares();

			if (_hostingEnv.IsProduction())
			{
				app.UseResponseCompression();
			}

			app.UseAuthentication();
			app.UseStaticFiles();
			app.UseMvc(routes =>
			{

				routes.MapSpaFallbackRoute(name: "spa-fallback", defaults: new { controller = "Home", action = "Index" });
			});
		}
    }
}
